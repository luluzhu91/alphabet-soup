/**
 * Javascript solution to Alphabet Soup problem by Lulu Zhu, 3/29/23
 * 
 * Utilized NodeJS
 * 
 * To run: type "node index.js" in console
 */

const fs = require('fs');

/* Read input file, split each line, and save into array */
var text = fs.readFileSync('test-input.txt', 'utf8').split('\n');

/* Parse row number from first line */
var numRows = Number(text[0].split('x')[0]); 

/* Parse multidimensional array */
var wordsearch = [];
for(let i = 1; i <= numRows; i++){
    wordsearch.push(text[i].split(' '));
}

/* Parse words to be found */
var words = [];
for(let i = numRows + 1; i < text.length; i++){
    words.push(text[i]);
}

/* Find starting point - first letter in wordsearch */
for(index in words){
    /* Iterate through wordsearch */
    for(let i = 0; i < wordsearch.length; i++){
        for(let j = 0; j < wordsearch[i].length; j++){
            if(wordsearch[i][j] == words[index][0]){ 

                /* Search top direction */
                searchDirection(words[index], i, j, -1, 0);

                /* Search top right direction */
                searchDirection(words[index], i, j, -1, 1);

                /* Search right direction */
                searchDirection(words[index], i, j, 0, 1);

                /* Search bottom right direction */ 
                searchDirection(words[index], i, j, 1, 1);

                /* Search bottom direction */ 
                searchDirection(words[index], i, j, 1, 0);

                /* Search bottom left direction */
                searchDirection(words[index], i, j, 1, -1);

                /* Search left direction */
                searchDirection(words[index], i, j, 0, -1);

                /* Search top left direction */
                searchDirection(words[index], i, j, -1, -1);

            } 
        }
    }
}

/**
 * Helper function that takes word, start indicies, direction to traverse in and follows that direction to look for word
 * 
 * return: N/A, prints directly to console
 */
function searchDirection(word, startX, startY, rowChange, columnChange){

    var currX = startX + rowChange;
    var currY = startY + columnChange;
    
    /* Iterate through remaining letters of word */
    for (let i = 1; i < word.length; i++){

        /* Check if we hit the boundaries of the wordsearch */
        if(currX >= 0 && currX < wordsearch.length && currY >= 0 && currY < wordsearch[0].length){

             /* Check if letters match */
             if(wordsearch[currX][currY] === word[i]){
                 
                /* If this is the last letter, break */
                if(i == word.length - 1){
                    break;
                }

                /* Update current location */
                 currX += rowChange;
                 currY += columnChange;
                 continue;

             } else {
                 /* Next letter was not found*/
                 return;
             }
        } else{
            /* Hit boundaries and didn't find entire word */
            return;
        }
           
    }
    /* Found all letters, return the proper formatted string */
    var result = word + " " + startX + ":" + startY + " " + currX + ":" + currY;
    console.log(result);
}
